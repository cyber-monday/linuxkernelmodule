#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/slab.h>
#include <linux/module.h>
#include <linux/kdev_t.h>
#include <linux/fs.h>
#include <linux/cdev.h>
#include <linux/device.h>
#include<linux/slab.h>               
#include<linux/uaccess.h>           
#include <linux/ioctl.h>


struct msg_read{
    void* kaddress;
    uint64_t* content; // Init 0 from client-side
};

struct msg_write{
    void* kaddress;
    uint64_t* value;
};


#define IOCTL_INFOLEAK	_IO('U', 1)
#define IOCTL_RW_READ   0xffd4
#define IOCTL_RW_WRITE  0xffd5
#define IOCTL_RW_KALLOC  _IO('U', 4)

static uint64_t value = 0x4142434445464748;
 
dev_t dev = 0;
static struct class *dev_class;
static struct cdev etx_cdev;

/*
** Function Prototypes
*/
static int      __init etx_driver_init(void);
static void     __exit etx_driver_exit(void);
static int      etx_open(struct inode *inode, struct file *file);
static int      etx_release(struct inode *inode, struct file *file);
static ssize_t  etx_read(struct file *filp, char __user *buf, size_t len,loff_t * off);
static ssize_t  etx_write(struct file *filp, const char *buf, size_t len, loff_t * off);
static long     etx_ioctl(struct file *file, unsigned int cmd, unsigned long arg);
ssize_t krwx_write(struct file *, const char *, size_t, loff_t *);
ssize_t krwx_read(struct file *, char *, size_t, loff_t *);

/*
** File operation sturcture
*/
static struct file_operations fops =
{
        .owner          = THIS_MODULE,
        .read           = etx_read,
        .write          = etx_write,
        .open           = etx_open,
        .unlocked_ioctl = etx_ioctl,
        .release        = etx_release,
};

/*
** This function will be called when we open the Device file
*/
static int etx_open(struct inode *inode, struct file *file)
{
        return 0;
}

/*
** This function will be called when we close the Device file
*/
static int etx_release(struct inode *inode, struct file *file)
{
        return 0;
}

/*
** This function will be called when we read the Device file
*/
static ssize_t etx_read(struct file *filp, char __user *buf, size_t len, loff_t *off)
{
        pr_info("Read Function\n");
        return 0;
}

/*
** This function will be called when we write the Device file
*/
static ssize_t etx_write(struct file *filp, const char __user *buf, size_t len, loff_t *off)
{
        pr_info("Write function\n");
        return len;
}


int ioctl_rw_read(struct msg_read* read_msg){
    //pr_info("ioctl_rw::ioctl_rw_read\n"); 
    /* NOT TESTED BELOW (coding blindly) */
    //uint64_t val = *(uint64_t *)(read_msg->kaddress);
    pr_info("msg_read->kaddress --> 0x%016llx\n", (uint64_t)read_msg->kaddress);
    pr_info("msg_read->kaddress val --> 0x%016llx\n", *(uint64_t*)(read_msg->kaddress));
    // pr_info("val --> %llu\n", val);
    if( copy_to_user(read_msg->content, read_msg->kaddress, sizeof(uint64_t)) )
        return -EFAULT;

    // pr_info("msg_read->kaddress --> %p\n", read_msg->kaddress);
     
    return 0;
}

unsigned long ioctl_rw_write(struct msg_write* write_msg){
    //pr_info("ioctl_rw::ioctl_rw_write\n");
    //struct msg_write write_msg;
    // uint64_t data = 0x0;
    uint64_t val = 0;
    char* p = (char*)(write_msg->kaddress);
    char* pval = (char *)&val;
    size_t i = 0;

    if(copy_from_user( &val, write_msg->value, sizeof(uint64_t))){
        return -EFAULT;
    }
    pr_info("val => 0x%p\n", (void *)val);
    pr_info("write_msg->value => 0x%016llu\n", (uint64_t)write_msg->value);
    pr_info("write_msg->kaddress => 0x%016llu\n", (uint64_t)write_msg->kaddress);
    // memset(write_msg->kaddress, )
    
    for(i = 0; i < sizeof(uint64_t); i++){
        pr_info("c => %d\nwritten to %016llu\n\n", pval[i], (uint64_t)(p+i));
        *(p+i) = pval[i];
    }
    //*(uint64_t *)(write_msg->kaddress) = val;
    // if( copy_from_user((void *)&value, &(write_msg->value), sizeof(uint64_t)) )
    //     return -EFAULT;
    //*(uint64_t *)(write_msg->kaddress) = data;

    return 0;
}

/*
** This function will be called when we write IOCTL on the Device file
*/
static long etx_ioctl(struct file *file, unsigned int cmd, unsigned long arg)
{
    struct msg_read read_msg;
    struct msg_write msg_write;
    void* value2;
    void* k_res;
    switch(cmd) {
        case IOCTL_RW_WRITE:
            if(copy_from_user( &msg_write, (struct msg_write*) arg, sizeof(struct msg_write))){
                return -EFAULT;
            }    
            ioctl_rw_write(&msg_write);
            break;
                // if( copy_from_user(&value ,(int32_t*) arg, sizeof(value)) )
                // {
                //         pr_err("Data Write : Err!\n");
                // }
                // copy_to_user(arg, *value, sizeof(value));
                
        case IOCTL_RW_READ:
            if(copy_from_user( &read_msg, (struct msg_read*) arg, sizeof(struct msg_read))){
                return -EFAULT;
            }
                
            ioctl_rw_read(&read_msg);
            break;
        
        case IOCTL_INFOLEAK:
            value2 = &value;
			if (copy_to_user((uint64_t*) arg, &value2, sizeof(value))){
				return -EFAULT;
			}
            break;
        
        case IOCTL_RW_KALLOC:
            k_res = kmalloc(10, GFP_KERNEL);
            if( copy_to_user((void *)arg, &k_res, sizeof(void*)) ) 
                return -EFAULT;
            break;

        default:
            pr_info("Default\n");
            break;
    }
    return 0;
}
 
/*
** Module Init function
*/
static int __init etx_driver_init(void)
{
        /*Allocating Major number*/
        if((alloc_chrdev_region(&dev, 0, 1, "etx_Dev")) <0){
            pr_err("Cannot allocate major number\n");
            return -1;
        }
        pr_info("Major = %d Minor = %d \n",MAJOR(dev), MINOR(dev));
 
        /*Creating cdev structure*/
        cdev_init(&etx_cdev,&fops);
 
        /*Adding character device to the system*/
        if((cdev_add(&etx_cdev,dev,1)) < 0){
            pr_err("Cannot add the device to the system\n");
            goto r_class;
        }
 
        /*Creating struct class*/
        if((dev_class = class_create(THIS_MODULE,"etx_class")) == NULL){
            pr_err("Cannot create the struct class\n");
            goto r_class;
        }
 
        /*Creating device*/
        if((device_create(dev_class,NULL,dev,NULL,"linuxterrain")) == NULL){
            pr_err("Cannot create the Device\n");
            goto r_device;
        }
        return 0;
 
r_device:
        class_destroy(dev_class);
r_class:
        unregister_chrdev_region(dev,1);
        return -1;
}

/*
** Module exit function
*/
static void __exit etx_driver_exit(void)
{
        device_destroy(dev_class,dev);
        class_destroy(dev_class);
        cdev_del(&etx_cdev);
        unregister_chrdev_region(dev, 1);
        pr_info("Device Driver Remove...Done!!!\n");
}
 
module_init(etx_driver_init);
module_exit(etx_driver_exit);
 
MODULE_LICENSE("GPL");
MODULE_DESCRIPTION("R/W Driver COLMAN");
MODULE_VERSION("1.0");
