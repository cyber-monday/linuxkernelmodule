CONFIG_MODULE_SIG=n
obj-m := kernel_mod.o
KVERSION = $(shell uname -r)
PWDD = $(shell pwd)

all:
	make -C /lib/modules/$(KVERSION)/build M="$(PWD)" modules
clean:
	make -C /lib/modules/$(KVERSION)/build M="$(PWD)" clean
